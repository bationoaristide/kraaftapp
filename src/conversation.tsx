import React, { useRef } from 'react';
import { SafeAreaView, ScrollView, StatusBar } from 'react-native';

import { Message, User } from './assets/types';
import styles from './app.styles';
import {
  formatDateToString,
  formatUnixToDate,
  getUser,
  isCurrentUser,
} from './common/utils';

import MessageView from './message.view';
import TextMessageView from './text.message.view';

interface Props {
  currentUserId: string;
  messages: Message[];
  users: User[];
}

const Conversation = (props: Props) => {
  const scrollViewRef = useRef();
  const { messages, users, currentUserId } = props;

  const formatDate = 'M/DD/YYYY - h:mm:ss A';
  const imageType = 'image';

  // sorted by creation date
  messages.sort((a, b) => {
    return +formatUnixToDate(a.createdAt) - +formatUnixToDate(b.createdAt);
  });

  return (
    <SafeAreaView
      style={[
        styles.conversationScrollView,
        { paddingTop: StatusBar.currentHeight },
      ]}>
      <ScrollView
        ref={scrollViewRef}
        onContentSizeChange={() =>
          scrollViewRef?.current?.scrollToEnd({ animated: true })
        }>
        {messages.map((message) => {
          return (
            <MessageView
              key={message.id}
              type={message.type}
              url={message.type === imageType ? message.url : undefined}
              createdAt={formatDateToString(
                formatUnixToDate(message.createdAt),
                formatDate,
              )}
              user={getUser(message.senderId, users)}
              isCurrentUser={isCurrentUser(message.senderId, currentUserId)}
              message={
                <TextMessageView
                  users={users}
                  message={
                    message.type === imageType
                      ? message.caption
                      : message.content
                  }
                />
              }
            />
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Conversation;
