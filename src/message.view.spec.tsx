import React from 'react';
import renderer from 'react-test-renderer';
import { formatDateToString, formatUnixToDate, getUser } from './common/utils';

import MessageView from './message.view';
import TextMessageView from './text.message.view';

describe('<MessageView />', () => {
  it('Should render pure component', () => {
    const type = 'image';
    const senderId = 'cedric@kraaft.co';
    const createdAt = 1618056882696;
    const url = 'https://i.imgur.com/WRgTbTg.png';
    const caption = `Right @Maxime. Teams do all kinds of things to bother the competition. I've heard of teams having heated benches in the winter for themselves but not for the visitors.`;
    const users = [
      {
        id: 'maxime2@kraaft.co',
        username: 'Maxime',
      },
      {
        id: 'maxime@kraaft.co',
        username: 'Maxime Blanchard',
      },
      {
        id: 'cedric@kraaft.co',
        username: 'Cedric Boidin',
      },
    ];
    renderer.create(
      <MessageView
        type={type}
        url={url}
        createdAt={formatDateToString(
          formatUnixToDate(createdAt),
          'M/DD/YYYY - h:mm:ss A',
        )}
        user={getUser(senderId, users)}
        isCurrentUser
        message={<TextMessageView users={users} message={caption} />}
      />,
    );
  });
});
