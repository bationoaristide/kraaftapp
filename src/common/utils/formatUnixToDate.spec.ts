import { formatUnixToDate } from './formatUnixToDate';

describe('formatUnixToDate', () => {
  it('Should return new date correctly', () => {
    const date = 1618056833265;
    const result = formatUnixToDate(date);
    expect(result.toISOString()).toEqual('2021-04-10T12:13:53.265Z');
  });

  it('Should return invalid date from invalid unix', () => {
    const date = 1618056833265909;
    const result = formatUnixToDate(date);
    expect(result.toISOString()).toEqual('+053244-03-06T15:27:45.909Z');
  });
});
