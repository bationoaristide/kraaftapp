import { isCurrentUser } from './isCurrentUser';

describe('isCurrentUser', () => {
  it('Should return true', () => {
    const userId = 'maxime2@kraaft.co';
    const id = 'maxime2@kraaft.co';
    const result = isCurrentUser(id, userId);
    expect(result).toBeTruthy();
  });

  it('Should return false', () => {
    const userId = 'maxime2@kraaft.co';
    const id = 'cedric@kraaft.co';
    const result = isCurrentUser(id, userId);
    expect(result).toBeFalsy();
  });
});
