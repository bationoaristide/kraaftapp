import { getUser } from './getUser';

describe('getUser', () => {
  const users = [
    {
      id: 'maxime2@kraaft.co',
      username: 'Maxime',
    },
    {
      id: 'maxime@kraaft.co',
      username: 'Maxime Blanchard',
    },
    {
      id: 'cedric@kraaft.co',
      username: 'Cedric Boidin',
    },
  ];

  it('Should return user find to list', () => {
    const userId = 'maxime2@kraaft.co';
    const result = getUser(userId, users);
    expect(result).toEqual(users[0]);
  });

  it('Should return invalid user with invalid id', () => {
    const userId = '';
    const result = getUser(userId, users);
    expect(result).toEqual(undefined);
  });
});
