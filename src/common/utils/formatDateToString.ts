import moment from 'moment';

export const formatDateToString = (date: Date, format: string): string => {
  return moment(date).format(format);
};
