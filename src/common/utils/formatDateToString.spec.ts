import { formatDateToString } from './formatDateToString';

describe('formatDateToString', () => {
  it('Should return date format correctly', () => {
    const date = new Date(2018, 8, 22, 15, 0, 0);
    const result = formatDateToString(date, 'M/DD/YYYY - h:mm:ss A');
    expect(result).toEqual('9/22/2018 - 3:00:00 PM');
  });
});
