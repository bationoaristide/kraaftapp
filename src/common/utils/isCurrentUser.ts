export const isCurrentUser = (id: string, senderId: string): boolean => {
  return id === senderId;
};
