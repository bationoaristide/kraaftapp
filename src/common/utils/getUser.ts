import { User } from '../../assets/types';

export const getUser = (id: string, users: User[]): User | undefined => {
  return users.find((user) => id === user.id);
};
