import { User } from '../../assets/types';

export const formatStringToUserTag = (
  message: string,
  users: User[],
): Array<string> => {
  const datas = message.match(/("[^"]+"|[^"\s]+)/g) || [];
  return datas.map((item, i) => {
    const word = item;
    const wordNext = datas[i + 1];
    let find = null;
    if (word && wordNext) {
      find = users.find(
        (item) =>
          `@${item.username}`.toLowerCase() ===
          `${word}_${wordNext}`.toLowerCase(),
      );
      if (!find) {
        find = users.find(
          (item) =>
            `@${item.username}`.toLowerCase() === `${word}`.toLowerCase(),
        );
        if (!find) {
          find = users.find(
            (item) =>
              `@${item.username}.`.toLowerCase() === `${word}`.toLowerCase(),
          );
        }
      } else {
        delete datas[i + 1];
      }
    }
    if (find) {
      return `${find.id}`;
    }
    return `${word} `;
  });
};
