import colors from '../../../assets/color';

export const kindType = ['medium', 'small', 'tiny', 'micro'] as const;

export const kindColor = ['greyLight', 'grey', 'negative', 'positive'] as const;

export type KindType = typeof kindType[number];
export type KindColor = typeof kindColor[number];

export const kindValues: { [k in KindType]: number } = {
  medium: 16,
  small: 15,
  tiny: 12,
  micro: 11,
};

export const kindColorValues: { [k in KindColor]: string } = {
  greyLight: colors.MESSAGE_DATE,
  grey: colors.GREY,
  negative: colors.BLACK,
  positive: colors.MESSAGE_TAG,
};
