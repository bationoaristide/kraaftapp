import styled from 'styled-components/native';

import { KindColor, kindColorValues, KindType, kindValues } from './KindType';

type Props = {
  kind?: KindType;
  kindColor?: KindColor;
  isCurrentUser?: boolean;
  bold?: boolean;
};

export const Paragraph = styled.Text<Props>((props) => ({
  textAlign: props.isCurrentUser ? 'right' : 'left',
  fontWeight: props.bold ? 'bold' : undefined,
  fontSize: props.kind ? kindValues[props.kind as KindType] : undefined,
  color: props.kindColor
    ? kindColorValues[props.kindColor as KindColor]
    : undefined,
}));
