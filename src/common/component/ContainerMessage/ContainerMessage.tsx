import styled from 'styled-components/native';

export const ContainerMessage = styled.View<{
  isCurrentUser?: boolean;
}>((props) => ({
  margin: 10,
  minHeight: 80,
  width: '80%',
  alignSelf: props.isCurrentUser ? 'flex-end' : undefined,
}));
