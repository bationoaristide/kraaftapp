import styled from 'styled-components/native';

export const ImageMessage = styled.Image<{
  isCurrentUser?: boolean;
}>((props) => ({
  height: 300,
  borderTopStartRadius: props.isCurrentUser ? 15 : 0,
  borderTopEndRadius: props.isCurrentUser ? 0 : 15,
}));
