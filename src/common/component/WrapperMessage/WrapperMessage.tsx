import styled from 'styled-components/native';

import colors from '../../../assets/color';

export const WrapperMessage = styled.View<{
  isImage?: boolean;
  isCurrentUser?: boolean;
}>((props) => ({
  backgroundColor: props.isCurrentUser
    ? colors.MESSAGE_BACKGROUND_SELF
    : colors.MESSAGE_BACKGROUND_REPLY,
  padding: 10,
  borderRadius: 15,
  borderTopEndRadius: props.isImage || props.isCurrentUser ? 0 : undefined,
  borderTopStartRadius: props.isImage || !props.isCurrentUser ? 0 : undefined,
}));
