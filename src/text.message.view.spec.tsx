import React from 'react';
import renderer from 'react-test-renderer';

import TextMessageView from './text.message.view';

describe('<TextMessageView />', () => {
  it('Should render pure component', () => {
    const users = [
      {
        id: 'maxime2@kraaft.co',
        username: 'Maxime',
      },
      {
        id: 'maxime@kraaft.co',
        username: 'Maxime Blanchard',
      },
      {
        id: 'cedric@kraaft.co',
        username: 'Cedric Boidin',
      },
    ];
    const content =
      "Did you know @Cedric Boidin that the University of Iowa's locker room is painted pink? I wonder why?";

    renderer.create(<TextMessageView users={users} message={content} />);
  });

  it('Should render with empty params', () => {
    const content = '';
    renderer.create(<TextMessageView users={[]} message={content} />);
  });
});
