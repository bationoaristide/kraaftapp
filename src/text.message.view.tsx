import React from 'react';
import { User } from './assets/types';

import { Paragraph } from './common/component';
import { formatStringToUserTag } from './common/utils';

interface Props {
  message: string;
  users: User[];
}

const TextMessageView = ({ message, users }: Props) => {
  const userList = users.map((item) => {
    const user = { ...item };
    user.username = user.username.replace(' ', '_');
    return user;
  });

  const content = formatStringToUserTag(message, userList);

  return (
    <Paragraph>
      {content.map((word) => {
        const userTag = users.find((user) => user.id === word);

        if (userTag) {
          return (
            <Paragraph
              bold
              kindColor="positive">{`@${userTag.username} `}</Paragraph>
          );
        }
        return <Paragraph>{word}</Paragraph>;
      })}
    </Paragraph>
  );
};

export default TextMessageView;
