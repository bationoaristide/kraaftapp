const colors = {
  WHITE: '#ffffff',
  BUTTON_BACKGROUND: '#2d3436',
  TOOLBAR_BACKGROUND: '#0984e3',
  MESSAGE_BACKGROUND_SELF: '#55efc4',
  MESSAGE_BACKGROUND_REPLY: '#dfe6e9',
  MESSAGE_TAG: '#ff7675',
  MESSAGE_DATE: '#636e72',
  GREY: '#4B4B4B',
  BLACK: '#000000',
};
export default colors;
