import React from 'react';
import { View } from 'react-native';

import { User } from './assets/types';
import {
  Paragraph,
  ContainerMessage,
  WrapperMessage,
  ImageMessage,
} from './common/component';

interface Props {
  message: JSX.Element;
  createdAt: string;
  type: string;
  url: string | undefined;
  user: User | undefined;
  isCurrentUser: boolean;
}

const MessageView = ({
  type,
  url,
  createdAt,
  user,
  isCurrentUser,
  message,
}: Props) => {
  const imageType = 'image';
  return (
    <ContainerMessage isCurrentUser={isCurrentUser}>
      <Paragraph isCurrentUser={isCurrentUser} kind="tiny" kindColor="grey">
        {user?.username}
      </Paragraph>
      <View>
        {type === imageType ? (
          <>
            <ImageMessage isCurrentUser={isCurrentUser} source={{ uri: url }} />
            <WrapperMessage isImage isCurrentUser={isCurrentUser}>
              {message}
            </WrapperMessage>
          </>
        ) : (
          <WrapperMessage isCurrentUser={isCurrentUser}>
            {message}
          </WrapperMessage>
        )}
      </View>
      <Paragraph
        isCurrentUser={isCurrentUser}
        kind="micro"
        kindColor="greyLight">
        {createdAt}
      </Paragraph>
    </ContainerMessage>
  );
};

export default MessageView;
