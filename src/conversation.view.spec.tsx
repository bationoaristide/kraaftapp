import React from 'react';
import renderer from 'react-test-renderer';

import Conversation from './conversation';

describe('<Conversation />', () => {
  it('Should render pure component', () => {
    const currentUserId = 'maxime@kraaft.co';
    const users = [
      {
        id: 'maxime2@kraaft.co',
        username: 'Maxime',
      },
      {
        id: 'maxime@kraaft.co',
        username: 'Maxime Blanchard',
      },
      {
        id: 'cedric@kraaft.co',
        username: 'Cedric Boidin',
      },
    ];
    const messages = [
      {
        id: 'JOyCwtqoK',
        type: 'text',
        senderId: 'maxime@kraaft.co',
        createdAt: 1618056833265,
        content:
          "Did you know @Cedric Boidin that the University of Iowa's locker room is painted pink? I wonder why?",
      },
      {
        id: 'yTFQY-vpSu',
        type: 'text',
        senderId: 'cedric@kraaft.co',
        createdAt: 1618056839996,
        content:
          'I think I did hear something about that. I imagine it is an attempt to psych the other team out.',
      },
    ];

    renderer.create(<Conversation {...{ messages, users, currentUserId }} />);
  });
});
